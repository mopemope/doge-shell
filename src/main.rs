use std::io::{stdin, stdout, Write};
use std::process::Command;

use log::error;
// use std::env;

fn main() {
    // let want_bt = match env::var("RUST_BACKTRACE").as_ref().map(|x| x.as_str()) {
    //     Ok("1") | Ok("full") => true,
    //     _ => false,
    // };
    env_logger::init();

    loop {
        print!("> ");
        if let Err(err) = stdout().flush() {
            error!("error {}", err);
            return;
        }

        let mut input = String::new();
        stdin().read_line(&mut input).unwrap();

        let command = input.trim();

        let mut child = Command::new(command).spawn().unwrap();

        if let Err(err) = child.wait(){
            error!("error {}", err);
            return;
        }
    }
}
